plan(key:'AUICDNLEGACY', name:'AUI ADG CDN legacy atlaskit artifacts - Build', description:'Build the AUI ADG CDN atlaskit legacy artifacts') {
    project(key:'FAB', name:'Product Fabric')

    repository(name:'aui-adg-dist')

    label(name:'AUI')

    label(name:'build')

    label(name:'design-platform')

    trigger(type:'polling',description:'Check for changes', strategy:'periodically',frequency:'180') {
        repository(name:'aui-adg-dist')
    }
    stage(name:'Default Stage') {
        job(key:'JOB1', name:'Build distribution', description:'Copy the legacy resources into the correct location') {
            requirement(key:'os', condition:'matches', value:'Linux')

            artifactDefinition(name: 'aui-cdn-dist-artifacts', location: './legacy',
                    pattern: '**/*', shared: 'true')

            miscellaneousConfiguration(cleanupWorkdirAfterBuild:'true')

            task(type:'checkout',description:'Checkout AUI ADG distribution repository',
                    cleanCheckout:'true') {
                repository(name:'aui-adg-dist')
            }
        }
    }
    dependencies(triggerForBranches:'true')

    permissions() {
        anonymous(permissions:'read')
        loggedInUser(permissions:'read,build')
    }

	planMiscellaneous() {
	   planOwnership(owner:'pcurren')
	}
}

deployment(name:'AUI ADG CDN legacy atlaskit artifacts - Deploy', planKey:'FAB-AUICDNLEGACY',
    description:'Deploys the legacy atlaskit artifacts to the Micros (static) managed AUI CDN') {
 
    versioning(version:'release-${bamboo.planKey}-1',autoIncrementNumber:'true')

    environment(name:'Dev') {
        trigger(type:'afterSuccessfulPlan', description:'Automatically go to dev-west2')
        notification(type:'Deployment Failed', recipient:'user', user:'pcurren')

        task(type:'addRequirement',description:'Require Linux') {
            requirement(key:'os',condition:'equals',value:'Linux')
        }

        task(type:'cleanWorkingDirectory')

        task(type:'artifactDownload',description:'Download AUI ADG assets artifact', planKey:'FAB-AUICDNDIST') {
            artifact(name:'listing-artifacts', localPath:'./dist')
        }

        task(type:'checkout') {
            repository(name:'aui-adg-dist')
        }

        task(type:'script', description:'Deploy to dev-west2', scriptBody:'''
                ./bin/deploy-existing-distributions.sh dev-west2
            '''.stripIndent())
    }

    environment(name:'Staging') {
        trigger(type:'afterSuccessfulDeployment', environment:'Dev')
        notification(type:'Deployment Failed', recipient:'user',user:'pcurren')

        task(type:'addRequirement',description:'Require Linux') {
            requirement(key:'os',condition:'equals',value:'Linux')
        }

        task(type:'cleanWorkingDirectory')

        task(type:'artifactDownload',description:'Download AUI ADG assets artifact', planKey:'FAB-AUICDNDIST') {
            artifact(name:'listing-artifacts', localPath:'./dist')
        }

        task(type:'checkout') {
            repository(name:'aui-adg-dist')
        }

        task(type:'script', description:'Deploy to stg-west2', scriptBody:'''
                ./bin/deploy-existing-distributions.sh stg-west2
            '''.stripIndent())
    }

    environment(name:'Production') {
        notification(type:'Deployment Failed', recipient:'user',user:'pcurren')

        task(type:'addRequirement',description:'Require Linux') {
            requirement(key:'os',condition:'equals',value:'Linux')
        }

        task(type:'cleanWorkingDirectory')

        task(type:'artifactDownload',description:'Download AUI ADG assets artifact', planKey:'FAB-AUICDNDIST') {
            artifact(name:'listing-artifacts', localPath:'./dist')
        }

        task(type:'checkout') {
            repository(name:'aui-adg-dist')
        }

        task(type:'script', description:'Deploy to prod-west2', scriptBody:'''
                ./bin/deploy-existing-distributions.sh prod-west2
            '''.stripIndent())
    }

    permissions() {
        anonymous(permissions:'read')
        loggedInUser(permissions:'read,build')
    }   
}