#!/usr/bin/env bash

# A script to remove all of the individual version "Fabric AUI" checks from Pollinator
# These individual checks have been superseeded by a synthetic check that checks them all

set -e

echo 'Ensure pollidev login has been run first.'

uuids=($(pollidev search --no-headers "Fabric AUI HTTPS" | awk '{print $1}'))

for uuid in ${uuids[*]};
    do
        pollidev import -f ../monitoring/pollinator-checks.yml $uuid
        echo 'Imported the check for uuid ' $uuid
    done

echo 'The original checks have been written to ../monitoring/pollinator-checks.yml'

# Now delete each check
for uuid in ${uuids[*]};
    do
        pollidev delete -f ../monitoring/pollinator-checks.yml --yes $uuid
        echo 'Deleted the check for uuid ' $uuid
    done

echo 'You can remove ../monitoring/pollinator-checks.yml if you are happy you will not need these checks again'