#!/bin/bash

# A script to check out all the currently deployed versions of aui-adg-dist and build them into the
# correct directory structure ready for deploying to the Micros CDN.

# print commands (-x) and stop execution on error (-e)
set -xe

versions=(
    "5.6.4"
    "5.6.5"
    "5.6.6"
    "5.6.7-jira-1"
    "5.6.7"
    "5.6.8"
	"5.6.10"
	"5.6.11"
	"5.6.12"
	"5.6.13"
	"5.6.15"
	"5.6.16"
    "5.6.900"
    "5.6.901"
    "5.6.902"
    "5.6.903"
    "5.6.904"
    "5.6.905"
    "5.7.0"
    "5.7.1"
    "5.7.10"
    "5.7.11"
    "5.7.12"
    "5.7.13"
    "5.7.14"
    "5.7.15"
    "5.7.16"
    "5.7.17"
    "5.7.18"
    "5.7.19"
    "5.7.2"
    "5.7.20"
    "5.7.21"
    "5.7.22"
    "5.7.23"
    "5.7.24"
    "5.7.25"
    "5.7.26"
    "5.7.27"
    "5.7.28"
    "5.7.29"
    "5.7.3"
    "5.7.30"
    "5.7.31"
    "5.7.32"
    "5.7.33"
    "5.7.34"
    "5.7.35"
    "5.7.36"
    "5.7.37"
    "5.7.38"
    "5.7.39"
    "5.7.4"
    "5.7.40"
    "5.7.41"
    "5.7.43"
    "5.7.44"
    "5.7.45"
    "5.7.46"
    "5.7.47"
    "5.7.48"
    "5.7.5"
    "5.7.7"
    "5.7.8"
    "5.7.9"
    "5.7.907"
    "5.7.909"
    "5.8.0"
    "5.8.1"
    "5.8.10"
    "5.8.11"
    "5.8.12"
    "5.8.13"
    "5.8.14"
    "5.8.15"
    "5.8.16"
    "5.8.17"
    "5.8.18"
    "5.8.19"
    "5.8.20"
    "5.8.21"
    "5.8.22"
    "5.8.23"
    "5.8.24"
    "5.8.26"
    "5.8.27"
    "5.8.28"
    "5.8.29"
    "5.8.3"
    "5.8.4"
    "5.8.5"
    "5.8.7"
    "5.8.8"
    "5.8.9"
    "5.9.0-alpha001"
    "5.9.0-alpha002"
    "5.9.0-alpha003"
    "5.9.0-alpha005"
    "5.9.0-alpha006"
    "5.9.0-alpha007"
    "5.9.0-beta001"
    "5.9.0-beta002"
    "5.9.0-beta003"
    "5.9.0"
    "5.9.1"
    "5.9.11"
    "5.9.12"
    "5.9.13"
    "5.9.14"
    "5.9.15"
    "5.9.16"
    "5.9.17"
    "5.9.18"
    "5.9.19"
    "5.9.2"
    "5.9.20"
    "5.9.21"
    "5.9.22"
    "5.9.23"
    "5.9.24"
    "5.9.3"
    "5.9.4"
    "5.9.5"
    "5.9.6"
    "5.9.7"
    "5.9.8"
    "5.10.1"
    "6.0.0"
    "6.0.1"
    "6.0.2"
    "6.0.3"
    "6.0.4"
    "6.0.5"
    "6.0.6"
    "6.0.7"
    "6.0.8"
    "6.0.9"
)

git fetch git@bitbucket.org:atlassian/aui-adg-dist.git --tags

# Check all the versions exist as tags before beginning the checkout and copying process
for ver in ${versions[*]};
    do
        if GIT_DIR=./.git git show-ref --tags | egrep -q "refs/tags/$ver\$"
        then
            echo "Found tag '$ver'."
        else
            echo "Tag '$ver' not found, exiting!" >&2
            exit 1
        fi
    done

# Loop over each tag again, checking each out and copying to a directory structure matching the CDN
for ver in ${versions[*]};
    do
        git checkout $ver
        # make the directory matching the version
        mkdir -p "./dist/aui-adg/$ver"      

        if [ -d ./aui-next ]; then
            cp -a ./aui-next/* "./dist/aui-adg/$ver/"
        else
            cp -a ./aui/* "./dist/aui-adg/$ver/"
        fi

        echo "Copied the distribution for version '$ver'" >&2
    done

# Check if the branch is set
if [ -z "${bamboo_planRepository_branch}" ] 
then
    echo "Current build branch is not set, so using default of master"
    branch="master"
else
    branch="${bamboo_planRepository_branch}"
fi

# Now checkout the original branch again so we are back to the latest version of the service
# descriptor prior to deploy
git checkout $branch
