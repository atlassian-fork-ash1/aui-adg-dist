#!/bin/bash

# A script to create pollinator HTTP checks for a small set of the AUI ADG versions deployed on the CDN
# You need to have installed the Pollinator CLI (https://extranet.atlassian.com/display/OBSERVABILITY/How+To%3A+Pollinator+CLI#installation--421128575)
# and have performed pollidev login first
#
# Note that the check created here is to ensure the AUI CDN is available, at a high level. The 
# availability of individual AUI ADG versions is checked via a synthetic Pollinator check.
# (See create-pollinator-versions-check.sh)

# print commands (-x) and stop execution on error (-e)
set -xe

# Set a version (or versions) to create HTTP checks for
versions=(
    "5.9.4"
)

# For each version replace the ${version} in the template and create on Pollinator
for ver in ${versions[*]};
    do
        sed -e "s/\${version}/$ver/" ../monitoring/pollinator-cdn-http-check-template.yml | pollidev create --interactive=false --no-save -
    done