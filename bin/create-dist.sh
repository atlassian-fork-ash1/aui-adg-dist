#!/usr/bin/env bash

# print commands (-x) and stop execution on error (-e)
set -xe

if [ -z "$1" ]; then
    echo "Usage: $0 <git tag>"
    exit 1
fi

git fetch git@bitbucket.org:atlassian/aui-adg-dist.git --tags

if GIT_DIR=./.git git show-ref --tags | egrep -q "refs/tags/$1"
then
    echo "Found tag '$1'."
else
    echo "Tag '$1' not found, exiting!" >&2
    echo "In case you didn't enter a tag, run a custom build and override the variable 'release.version'." >&2
    exit 1
fi

git checkout $1

# make the directory matching the version
mkdir -p "./dist/aui-adg/$1"
cp -a ./aui "./dist/aui-adg/$1"
