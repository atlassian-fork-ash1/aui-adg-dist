class OriginalAuiCdnSimulation extends AuiResourcesBase {
  override def getProtocol: String = "https"

  override def getAddress: String = "aui-cdn.atlassian.com"

  override def getResource: String = "js/aui.min.js"
}
