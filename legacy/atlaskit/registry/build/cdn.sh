set -e
set -x

# Need to have set AWS_ACCESS_KEY AWS_SECRET_KEY

java -jar ./prebake-distributor-runner.jar \
--step=resources \
--s3-bucket=$S3_BUCKET \
--s3-key-prefix="$S3_KEY_PREFIX" \
--s3-gz-key-prefix="$S3_GZ_KEY_PREFIX" \
--compress=css,js,svg,ttf,html,json,ico,eot,otf \
--pre-bake-bundle=./cdn.zip
