import {
  define,
  prop,
  h, // eslint-disable-line no-unused-vars
} from 'skatejs';
import distanceInWordsToNow from 'date-fns/distance_in_words_to_now';

export default define('atlaskit-registry-relative-date', {
  props: {
    iso: prop.string({
      attribute: true,
    }),
  },
  render(elem) {
    const timeAgo = distanceInWordsToNow(elem.iso);
    return <span>{timeAgo} ago</span>;
  },
});
