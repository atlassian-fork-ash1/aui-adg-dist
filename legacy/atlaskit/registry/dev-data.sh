#!/bin/sh
# This sets you up with some demo data, with most components removed so dev build time is fast.
set -e
mkdir -p ./api
curl http://aui-cdn.atlassian.com/atlaskit/registry/api/full.json -o ./api/full.json
node -e "var fs = require('fs'); var json = JSON.parse(fs.readFileSync('./api/full.json', { encoding: 'utf8' })); json.components = json.components.slice(0, 2).map(function(c) { c.versions = c.versions.slice(0, 2); return c; }); fs.writeFileSync('./api/full.json', JSON.stringify(json));"
